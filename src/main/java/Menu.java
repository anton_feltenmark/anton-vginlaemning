import java.util.ArrayList;

public class Menu {
    public void gameMenu() {

        Game game = new Game(new ArrayList<>());
        Tournament tournament = new Tournament();
        boolean power = true;

        while(power){
            System.out.println("1: Play\n" + "2: View tournament history\n"
                    + "3: Get playerstats\n" + "4: End program");
            int choice = Game.scanner.nextInt();
            switch (choice){
                case 1 -> game.gameON();
                case 2 -> tournament.printTournament();
                case 4 -> power = false;
            }

        }


    }
}

