import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class MatchResult {
    private Player firstPlace;
    private Player secondPlace;
    private Player thirdPlace;
    private Player fourthPlace;
    private String localDateTime;
    private DateTimeFormatter myFormat;





    public void setMatchResult(String localDateTime,Player firstPlace,Player secondPlace,Player thirdPlace,Player fourthPlace){
        this.localDateTime = localDateTime;
        this.firstPlace = firstPlace;
        this.secondPlace = secondPlace;
        this.thirdPlace = thirdPlace;
        this.fourthPlace = fourthPlace;
    }
    public DateTimeFormatter getMyFormat(){
        return DateTimeFormatter.ofPattern("yyyy/MM-dd HH:mm");
    }
    public String getDateAndTime(LocalDateTime localDateTime){

        return LocalDateTime.now().format(getMyFormat());

    }

    @Override
    public String toString() {
        return "Date: " + localDateTime +
                "\n Tournamentresult:" +
                "\n firstPlace: " + firstPlace.getName() +
                "\n secondPlace: " + secondPlace.getName() +
                "\n thirdPlace: " + thirdPlace.getName() +
                "\n fourthPlace: " + fourthPlace.getName() +
                "\n -------------";
    }

    public Player getFirstPlace() {
        return firstPlace;
    }

    public Player getFourthPlace() {
        return fourthPlace;
    }

    public Player getThirdPlace() {
        return thirdPlace;
    }

    public Player getSecondPlace() {
        return secondPlace;
    }

}
