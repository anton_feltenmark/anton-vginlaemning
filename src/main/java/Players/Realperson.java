package Players;

import java.util.Scanner;

public class Realperson implements Tactic{
    @Override
    public Moves tactic(Object p) {
        System.out.println("Choose your move: \n" + "1: Sciccors\n" + "2: Paper\n" + "3: Rock");
        Scanner scanner = new Scanner(System.in);
        int move = scanner.nextInt();

        return switch (move){
            case 1 -> Moves.SCISSORS;
            case 2 -> Moves.PAPER;
            default -> Moves.ROCK;
        };


    }
}
