package Players;

import java.util.Random;

public class DumbComp implements Tactic{
    @Override
    public Moves tactic(Object p) {
        Random random = new Random();
        int move = random.nextInt(3) + 1 ;

        return switch (move){
            case 1 -> Moves.PAPER;
            case 2 -> Moves.ROCK;
            default -> Moves.SCISSORS;

        };

    }
}
