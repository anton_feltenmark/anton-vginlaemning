package Players;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VowelsAddictComp implements Tactic{


    @Override
    public Moves tactic(Object p) {
        String playerName = p.getClass().getName();

        String regex = "[bcdfghjklmnpqrstvwxzBCDFGHJKLMNPQRSTVWXZ]";



        //Pattern pattern = Pattern.compile(regex);
        //Matcher matcher = pattern.matcher(playerName);
        playerName.replaceAll(regex ,"");
        String replaceName = playerName.replaceAll(regex,"");


        if (replaceName.length()<= 2) {
            return Moves.ROCK;
        } else if (replaceName.length()< 4){
            return Moves.SCISSORS;
        } else {
            return Moves.PAPER;
        }

    }

}
